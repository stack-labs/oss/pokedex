# Pokedex

First generation, __1 to 151__.

* `pokedex.json` contains pokemon data (names, id, attributes and more)
* `images` contains great pictures of pokémons
* `thumbnails` contains small pictures of pokémons (easy to load)

## How to access [developers]

* `https://gitlab.com/stack-labs/oss/pokedex/-/raw/main/pokedex.json` access to raw json
* `https://gitlab.com/stack-labs/oss/pokedex/-/raw/main/images/POKEMON_INDEX.png` where `POKEMON_INDEX` is formatted on 3 characters : 00X, 0XX, XXX
* `https://gitlab.com/stack-labs/oss/pokedex/-/raw/main/thumbnails/POKEMON_INDEX.png` where `POKEMON_INDEX` is formatted on 3 characters : 00X, 0XX, XXX

Example to access on [Bulbizaur](https://gitlab.com/stack-labs/oss/pokedex/-/raw/main/images/001.png) image.

## Copyright Notice
Please note everything in repository are copyrighted by the __Pokémon Company and its affiliates__.<br>This repository is merely a compilation of data collected by the editors of Bulbapedia. (Source from https://github.com/fanzeyi)
